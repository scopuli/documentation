# documentation
## introduction
This project seeks to address the problem of resource imbalance.

Altruism - (selfless) concern for the well-being of others - is a core part of human nature, and it could be argued a corner-stone of the development
of human society and all that flows from it: culture, technology and the means to sustain life itself.

We see this all the time, in our interactions large and small: from passing the salt in a diner, helping someone cross a busy road through to donating financially
or voluntary work.  We do these things without thought of obligation or reward, it is what we are.  If I pick-up a child's dropped toy, the parent would not
expect to presented with a bill for services rendered.


Commercial exchange

## tech
* Resource exchange
* Channels: apps, web & mobile
* Miners, discovery
* Bank (??!) ~ blockchain?
